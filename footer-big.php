<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */
?>

<footer id="main-footer-id" class="main-footer">
<div class="grid-container">
<div class="grid-x align-middle">
	<div class="cell medium-6 large-2 footer-logo-cell">
		<div class="footer-logo-wrap">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 86 17.26" id="footer-logo"><title>m-logo</title><path d="M1466.79,1747.48c0.18-.22.33-0.41,0.49-0.58a4.23,4.23,0,0,1,2.33-1.3,5.66,5.66,0,0,1,2.47,0,3.33,3.33,0,0,1,2.68,2.73,7.57,7.57,0,0,1,.17,1.45c0,2.51,0,5,.06,7.54,0,0.12,0,.25,0,0.4-0.72,0-1.41,0-2.14,0,0-.18,0-0.32,0-0.47,0-2.24,0-4.48,0-6.72a7.83,7.83,0,0,0-.15-1.45,2,2,0,0,0-2-1.76,2.8,2.8,0,0,0-2.13.55,3.16,3.16,0,0,0-1,1.6,6.37,6.37,0,0,0-.24,1.9q0,2.94,0,5.89v0.44c-0.71,0-1.38,0-2.1,0v-0.46c0-2.23,0-4.45,0-6.68a8.55,8.55,0,0,0-.19-1.56,2,2,0,0,0-1.92-1.68,2.76,2.76,0,0,0-2.43.75,3.3,3.3,0,0,0-.87,1.76,8.9,8.9,0,0,0-.15,1.53c0,1.95,0,3.9,0,5.85v0.48h-2.07v-12h2.06v1.43l0.12,0.07a1.3,1.3,0,0,1,.1-0.2,3.49,3.49,0,0,1,2.2-1.4,5.7,5.7,0,0,1,2.65,0,3,3,0,0,1,2,1.59l0.16,0.27" transform="translate(-1457.54 -1741.06)"/><path d="M1528.16,1741.06a6.76,6.76,0,0,1,3.3.76,5.86,5.86,0,0,1,2.63,3.19,12.17,12.17,0,0,1,.72,3.94,18.78,18.78,0,0,1,0,2.1,10,10,0,0,1-1.23,4.38,5.5,5.5,0,0,1-3.9,2.75,7.44,7.44,0,0,1-3.47-.14,5.51,5.51,0,0,1-3.44-2.9,9.29,9.29,0,0,1-1-3.38,20,20,0,0,1-.14-2.44,12.15,12.15,0,0,1,.8-4.49,5.81,5.81,0,0,1,2.52-3,6.24,6.24,0,0,1,2.62-.75l0.6,0m-4.26,8.56a12.43,12.43,0,0,0,.47,3.6,5.11,5.11,0,0,0,1.25,2.21,3.88,3.88,0,0,0,4.25.65,3.84,3.84,0,0,0,1.77-1.79,8.83,8.83,0,0,0,.88-3.74c0-.79,0-1.58,0-2.37a8.16,8.16,0,0,0-.55-2.56,4.28,4.28,0,0,0-1.36-1.91,3.86,3.86,0,0,0-3-.71,3.46,3.46,0,0,0-2.17,1.17,4.8,4.8,0,0,0-.92,1.63,11.69,11.69,0,0,0-.55,3.82" transform="translate(-1457.54 -1741.06)"/><path d="M1506,1749.39l-2-.09a3.46,3.46,0,0,1,.55-1.82,4.24,4.24,0,0,1,2.31-1.72,7.41,7.41,0,0,1,2.9-.3,7.79,7.79,0,0,1,1.37.22,3.2,3.2,0,0,1,2.57,2.84,8.39,8.39,0,0,1,.09,1.2c0,2.5,0,5,0,7.51v0.49c-0.61,0-1.19,0-1.82,0l-0.09-1.5-0.08-.06a1.64,1.64,0,0,1-.1.19,3.76,3.76,0,0,1-2.42,1.53,6,6,0,0,1-3.16-.13,3.71,3.71,0,0,1-2.53-3.52,3.09,3.09,0,0,1,.7-2.13,3.61,3.61,0,0,1,1.34-.95,10.8,10.8,0,0,1,3.09-.76l2.91-.32a2.22,2.22,0,0,0-1.4-2.73,4.47,4.47,0,0,0-2.74.08,2.13,2.13,0,0,0-1.41,1.72s0,0.08,0,.11,0,0.07,0,.16m5.81,2.39c-1.23.14-2.42,0.25-3.59,0.41a4.4,4.4,0,0,0-1.46.46,1.67,1.67,0,0,0-.84,2.14A1.72,1.72,0,0,0,1507,1756a3.75,3.75,0,0,0,2.51,0,3,3,0,0,0,1.76-1.39,4.82,4.82,0,0,0,.49-2.79" transform="translate(-1457.54 -1741.06)"/><path d="M1487.54,1757.7h-1.79a14.11,14.11,0,0,1-.14-1.54,0.91,0.91,0,0,0-.13.12,3.87,3.87,0,0,1-2.51,1.59,6.47,6.47,0,0,1-2.5,0,3.69,3.69,0,0,1-3.07-3.07,3.19,3.19,0,0,1,.55-2.65,3.71,3.71,0,0,1,1.48-1.08,9.1,9.1,0,0,1,2.47-.66c1.12-.15,2.24-0.26,3.36-0.39l0.14,0a2.19,2.19,0,0,0-1.65-2.77,4.38,4.38,0,0,0-2.45.11,2.15,2.15,0,0,0-1.46,1.72c0,0.09,0,.17-0.07.3l-2-.09a3.64,3.64,0,0,1,1.5-2.83,5.35,5.35,0,0,1,2.95-1,8.72,8.72,0,0,1,2.55.16,3.27,3.27,0,0,1,2.74,3,7.33,7.33,0,0,1,.09,1.16c0,2.49,0,5,0,7.47v0.49h0Zm-2-5.87-0.1,0c-1.15.12-2.29,0.23-3.43,0.38a4,4,0,0,0-1.79.63,1.53,1.53,0,0,0-.69,1.39,1.82,1.82,0,0,0,1.32,1.78,3.74,3.74,0,0,0,2.54-.06,3,3,0,0,0,1.33-.89,3.51,3.51,0,0,0,.82-2.13c0-.36,0-0.72,0-1.07" transform="translate(-1457.54 -1741.06)"/><polygon points="83.86 2.28 80 2.28 80 0.33 86 0.33 86 16.94 83.86 16.94 83.86 2.28 83.86 2.28"/><path d="M1492.75,1757.7h-2.08v-12c0.31-.09,5.75-0.09,6.11,0v2c-1.34,0-2.67,0-4,0v9.93h0Z" transform="translate(-1457.54 -1741.06)"/><path d="M1498.83,1745.68h2c0.07,0.28.09,11.54,0,12h-2v-12h0Z" transform="translate(-1457.54 -1741.06)"/></svg></a>
		</div>
		<div class="grid-x footer-sm-links">
			<div class="cell sm-links-cell">
				<a href="https://www.facebook.com/mariazeroone/" target="_blank"><span><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 27.22 27.22"><title>facebook</title><path d="M1460.78,1736.32h-24.21a1.5,1.5,0,0,0-1.5,1.5V1762a1.5,1.5,0,0,0,1.5,1.5h13V1753h-3.55v-4.11h3.55v-3c0-3.52,2.15-5.43,5.28-5.43a29.1,29.1,0,0,1,3.17.16v3.67h-2.18c-1.71,0-2,.81-2,2v2.62h4.07l-0.53,4.11h-3.54v10.54h6.94a1.5,1.5,0,0,0,1.5-1.5v-24.21A1.5,1.5,0,0,0,1460.78,1736.32Z" transform="translate(-1435.06 -1736.32)" style="fill:#fff"/></svg></span></a>
			</div>
			<div class="cell sm-links-cell">
				<a href="https://twitter.com/MariaZeroOne" target="_blank"><span><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 33.5 27.22"><title>twitter</title><path d="M1544,1763.54c12.64,0,19.55-10.47,19.55-19.55,0-.3,0-0.59,0-0.89a14,14,0,0,0,3.43-3.56,13.72,13.72,0,0,1-3.95,1.08,6.9,6.9,0,0,0,3-3.8,13.77,13.77,0,0,1-4.36,1.67,6.88,6.88,0,0,0-11.71,6.27,19.51,19.51,0,0,1-14.16-7.18,6.88,6.88,0,0,0,2.13,9.17,6.82,6.82,0,0,1-3.12-.86V1746a6.87,6.87,0,0,0,5.51,6.74,6.86,6.86,0,0,1-3.1.12,6.88,6.88,0,0,0,6.42,4.77,13.79,13.79,0,0,1-8.53,2.95,14,14,0,0,1-1.64-.1,19.46,19.46,0,0,0,10.53,3.08" transform="translate(-1533.5 -1736.32)" style="fill:#fff"/></svg></span></a>
			</div>
			<div class="cell sm-links-cell">
				<a href="https://www.instagram.com/mariazeroone/?hl=en" target="_blank"><span><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 27.22 27.22"><title>insta</title><path d="M1497.89,1738.77c3.63,0,4.06,0,5.5.08a7.53,7.53,0,0,1,2.53.47,4.51,4.51,0,0,1,2.58,2.58,7.53,7.53,0,0,1,.47,2.53c0.07,1.44.08,1.87,0.08,5.5s0,4.06-.08,5.5a7.53,7.53,0,0,1-.47,2.53,4.51,4.51,0,0,1-2.58,2.58,7.53,7.53,0,0,1-2.53.47c-1.43.07-1.87,0.08-5.5,0.08s-4.06,0-5.5-.08a7.53,7.53,0,0,1-2.53-.47,4.51,4.51,0,0,1-2.58-2.58,7.53,7.53,0,0,1-.47-2.53c-0.07-1.44-.08-1.87-0.08-5.5s0-4.06.08-5.5a7.53,7.53,0,0,1,.47-2.53,4.51,4.51,0,0,1,2.58-2.58,7.53,7.53,0,0,1,2.53-.47c1.44-.07,1.87-0.08,5.5-0.08m0-2.45c-3.7,0-4.16,0-5.61.08a10,10,0,0,0-3.3.63,7,7,0,0,0-4,4,10,10,0,0,0-.63,3.3c-0.07,1.45-.08,1.91-0.08,5.61s0,4.16.08,5.61a10,10,0,0,0,.63,3.3,7,7,0,0,0,4,4,10,10,0,0,0,3.3.63c1.45,0.07,1.91.08,5.61,0.08s4.16,0,5.61-.08a10,10,0,0,0,3.3-.63,7,7,0,0,0,4-4,10,10,0,0,0,.63-3.3c0.07-1.45.08-1.91,0.08-5.61s0-4.16-.08-5.61a10,10,0,0,0-.63-3.3,7,7,0,0,0-4-4,10,10,0,0,0-3.3-.63c-1.45-.07-1.91-0.08-5.61-0.08h0Z" transform="translate(-1484.28 -1736.32)" style="fill:#fff"/><path d="M1497.89,1742.94a7,7,0,1,0,7,7A7,7,0,0,0,1497.89,1742.94Zm0,11.52a4.54,4.54,0,1,1,4.54-4.54A4.54,4.54,0,0,1,1497.89,1754.47Z" transform="translate(-1484.28 -1736.32)" style="fill:#fff"/><circle cx="20.87" cy="6.34" r="1.63" style="fill:#fff"/></svg></span></a>
			</div>

		</div>


	</div>
	<div class="cell medium-6 large-5 footer-nav-info-cell">
	<div class="grid-x footer-nav-info">
		<div class="cell">
			<span>Maria 01, Lapinlahdenkatu 16, 00180 Helsinki</span>
		</div>
		<div class="cell">
			<span>hello@maria.io</span>
		</div>

	</div>

	</div>
	<div class="cell footer-links-wrapper medium-6 large-auto">
<div class="grid-x footer-nav-links footer-header-links">
	<div class="cell">
		<a href="<?php echo esc_url( home_url( '/about' ) ); ?>" rel="home"><span>About
	</span></a>
	</div>

	<div class="cell">
		<a href="<?php echo esc_url( home_url( '/campus' ) ); ?>" rel="home"><span>Campus
	</span></a>
	</div>
	<div class="cell">
		<a href="<?php echo esc_url( home_url( '/community' ) ); ?>" rel="home"><span>Community
	</span></a>
	</div>
	<div class="cell compress-link compress-link-margin">
		<a href="<?php echo esc_url( home_url( '/work-with-us' ) ); ?>" rel="home"><span>Work with us
	</span></a>
	</div>
	<div class="cell compress-link">
		<a href="<?php echo esc_url( home_url( '/become-a-member' ) ); ?>" rel="home"><span>BECOME A MEMBER
	</span></a>
	</div>
	<div class="cell compress-link">
		<span>Member Login
	</span>
	</div>
	</div>
	</div>
	<div class="cell footer-links-wrapper medium-6 large-auto">
<div class="grid-x footer-nav-links">
	<div class="cell">
		<a href="<?php echo esc_url( home_url( '/contact' ) ); ?>" rel="home"><span>Contact Us
	</span></a>
	</div>
	<div class="cell">
		<a href="https://drive.google.com/drive/u/1/folders/0ByXN2wgb7KZGanYtYzRCTmQ0RzQ
" rel="home" target="_blank"><span>Presskit
	</span></a>
	</div>
	<div class="cell">
		<span>Code of conduct
	</span>
	</div>
	<div class="cell">
		<span>Impact Report 2018
	</span>
	</div>
	</div>
	</div>
<!-- <div class="cell shrink">
<div class="grid-x footer-sm-links">
	<div class="cell sm-links-cell">
		<span><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 27.22 27.22"><title>facebook</title><path d="M1460.78,1736.32h-24.21a1.5,1.5,0,0,0-1.5,1.5V1762a1.5,1.5,0,0,0,1.5,1.5h13V1753h-3.55v-4.11h3.55v-3c0-3.52,2.15-5.43,5.28-5.43a29.1,29.1,0,0,1,3.17.16v3.67h-2.18c-1.71,0-2,.81-2,2v2.62h4.07l-0.53,4.11h-3.54v10.54h6.94a1.5,1.5,0,0,0,1.5-1.5v-24.21A1.5,1.5,0,0,0,1460.78,1736.32Z" transform="translate(-1435.06 -1736.32)" style="fill:#fff"/></svg></span>
	</div>
	<div class="cell sm-links-cell">
		<span><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 33.5 27.22"><title>twitter</title><path d="M1544,1763.54c12.64,0,19.55-10.47,19.55-19.55,0-.3,0-0.59,0-0.89a14,14,0,0,0,3.43-3.56,13.72,13.72,0,0,1-3.95,1.08,6.9,6.9,0,0,0,3-3.8,13.77,13.77,0,0,1-4.36,1.67,6.88,6.88,0,0,0-11.71,6.27,19.51,19.51,0,0,1-14.16-7.18,6.88,6.88,0,0,0,2.13,9.17,6.82,6.82,0,0,1-3.12-.86V1746a6.87,6.87,0,0,0,5.51,6.74,6.86,6.86,0,0,1-3.1.12,6.88,6.88,0,0,0,6.42,4.77,13.79,13.79,0,0,1-8.53,2.95,14,14,0,0,1-1.64-.1,19.46,19.46,0,0,0,10.53,3.08" transform="translate(-1533.5 -1736.32)" style="fill:#fff"/></svg></span>
	</div>
	<div class="cell sm-links-cell">
		<span><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 27.22 27.22"><title>insta</title><path d="M1497.89,1738.77c3.63,0,4.06,0,5.5.08a7.53,7.53,0,0,1,2.53.47,4.51,4.51,0,0,1,2.58,2.58,7.53,7.53,0,0,1,.47,2.53c0.07,1.44.08,1.87,0.08,5.5s0,4.06-.08,5.5a7.53,7.53,0,0,1-.47,2.53,4.51,4.51,0,0,1-2.58,2.58,7.53,7.53,0,0,1-2.53.47c-1.43.07-1.87,0.08-5.5,0.08s-4.06,0-5.5-.08a7.53,7.53,0,0,1-2.53-.47,4.51,4.51,0,0,1-2.58-2.58,7.53,7.53,0,0,1-.47-2.53c-0.07-1.44-.08-1.87-0.08-5.5s0-4.06.08-5.5a7.53,7.53,0,0,1,.47-2.53,4.51,4.51,0,0,1,2.58-2.58,7.53,7.53,0,0,1,2.53-.47c1.44-.07,1.87-0.08,5.5-0.08m0-2.45c-3.7,0-4.16,0-5.61.08a10,10,0,0,0-3.3.63,7,7,0,0,0-4,4,10,10,0,0,0-.63,3.3c-0.07,1.45-.08,1.91-0.08,5.61s0,4.16.08,5.61a10,10,0,0,0,.63,3.3,7,7,0,0,0,4,4,10,10,0,0,0,3.3.63c1.45,0.07,1.91.08,5.61,0.08s4.16,0,5.61-.08a10,10,0,0,0,3.3-.63,7,7,0,0,0,4-4,10,10,0,0,0,.63-3.3c0.07-1.45.08-1.91,0.08-5.61s0-4.16-.08-5.61a10,10,0,0,0-.63-3.3,7,7,0,0,0-4-4,10,10,0,0,0-3.3-.63c-1.45-.07-1.91-0.08-5.61-0.08h0Z" transform="translate(-1484.28 -1736.32)" style="fill:#fff"/><path d="M1497.89,1742.94a7,7,0,1,0,7,7A7,7,0,0,0,1497.89,1742.94Zm0,11.52a4.54,4.54,0,1,1,4.54-4.54A4.54,4.54,0,0,1,1497.89,1754.47Z" transform="translate(-1484.28 -1736.32)" style="fill:#fff"/><circle cx="20.87" cy="6.34" r="1.63" style="fill:#fff"/></svg></span>
	</div>

</div>

</div> -->
</div>
<div class="footer-copy-right">
<span>© Maria 01, 2019 (	<a class="white-link" href="<?php echo esc_url( home_url( '/privacy-policy-terms-of-use' ) ); ?>" rel="home">Privacy Policy & Terms of Use</a>)</span>
</div>
</div>
</footer>
		</div>
<?php wp_footer(); ?>


</body>
</html>
