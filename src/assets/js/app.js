// import $ from 'jquery';
// import whatInput from 'what-input';

// window.$ = $;

// import Foundation from 'foundation-sites';
// // If you want to pick and choose which modules to include, comment out the above and uncomment
// // the line below
// //import './lib/foundation-explicit-pieces';
//
// $(document).foundation();


// import Splitting from "splitting";
// import medium from "./modules/medium";
// import directory from "./modules/directory";
 // import Marquee3k from 'marquee3000';
import navs from "./modules/navs";
import {
  headroom
} from "./modules/headroom";

// import AOS from 'aos';
import slider from './modules/slider';
// import logoslider from './modules/logo-slider';
headroom();

///REMOVE LOADING POSITIONER/////
setTimeout(function(){

document.querySelector(".main-header").classList.remove("loading--fix");

}, 100);



import Rellax from 'rellax';
import SmoothScroll from "smooth-scroll";

import SwupJsPlugin from '@swup/js-plugin';
import 'lazysizes';
// lazySizes.cfg.expFactor = '3';
// lazySizes.cfg.expand = '700';
var imagesLoaded = require('imagesloaded');

/*
This plugin extends lazySizes to lazyLoad:
background images, videos/posters and scripts
Background-Image:
For background images, use data-bg attribute:
<div class="lazyload" data-bg="bg-img.jpg"></div>
 Video:
 For video/audio use data-poster and preload="none":
 <video class="lazyload" data-poster="poster.jpg" preload="none">
 <!-- sources -->
 </video>
 Scripts:
 For scripts use data-script:
 <div class="lazyload" data-script="module-name.js"></div>
 Script modules using require:
 For modules using require use data-require:
 <div class="lazyload" data-require="module-name"></div>
*/

(function(window, factory) {
	var globalInstall = function(){
		factory(window.lazySizes);
		window.removeEventListener('lazyunveilread', globalInstall, true);
	};

	factory = factory.bind(null, window, window.document);

	if(typeof module == 'object' && module.exports){
		factory(require('lazysizes'));
	} else if(window.lazySizes) {
		globalInstall();
	} else {
		window.addEventListener('lazyunveilread', globalInstall, true);
	}
}(window, function(window, document, lazySizes) {
	/*jshint eqnull:true */
	'use strict';
	var bgLoad, regBgUrlEscape;
	var uniqueUrls = {};

	if(document.addEventListener){
		regBgUrlEscape = /\(|\)|\s|'/;

		bgLoad = function (url, cb){
			var img = document.createElement('img');
			img.onload = function(){
				img.onload = null;
				img.onerror = null;
				img = null;
				cb();
			};
			img.onerror = img.onload;

			img.src = url;

			if(img && img.complete && img.onload){
				img.onload();
			}
		};

		addEventListener('lazybeforeunveil', function(e){
			if(e.detail.instance != lazySizes){return;}

			var tmp, load, bg, poster;
			if(!e.defaultPrevented) {

				if(e.target.preload == 'none'){
					e.target.preload = 'auto';
				}

				tmp = e.target.getAttribute('data-link');
				if(tmp){
					addStyleScript(tmp, true);
				}

				// handle data-script
				tmp = e.target.getAttribute('data-script');
				if(tmp){
					addStyleScript(tmp);
				}

				// handle data-require
				tmp = e.target.getAttribute('data-require');
				if(tmp){
					if(lazySizes.cfg.requireJs){
						lazySizes.cfg.requireJs([tmp]);
					} else {
						addStyleScript(tmp);
					}
				}

				// handle data-bg
				bg = e.target.getAttribute('data-bg');
				if (bg) {
					e.detail.firesLoad = true;
					load = function(){
						e.target.style.backgroundImage = 'url(' + (regBgUrlEscape.test(bg) ? JSON.stringify(bg) : bg ) + ')';
						e.detail.firesLoad = false;
						lazySizes.fire(e.target, '_lazyloaded', {}, true, true);
					};

					bgLoad(bg, load);
				}

				// handle data-poster
				poster = e.target.getAttribute('data-poster');
				if(poster){
					e.detail.firesLoad = true;
					load = function(){
						e.target.poster = poster;
						e.detail.firesLoad = false;
						lazySizes.fire(e.target, '_lazyloaded', {}, true, true);
					};

					bgLoad(poster, load);

				}
			}
		}, false);

	}

	function addStyleScript(src, style){
		if(uniqueUrls[src]){
			return;
		}
		var elem = document.createElement(style ? 'link' : 'script');
		var insertElem = document.getElementsByTagName('script')[0];

		if(style){
			elem.rel = 'stylesheet';
			elem.href = src;
		} else {
			elem.src = src;
		}
		uniqueUrls[src] = true;
		uniqueUrls[elem.src || elem.href] = true;
		insertElem.parentNode.insertBefore(elem, insertElem);
	}
}));

// import lozad from 'lozad'



var scroll = new SmoothScroll('a[href*="#"]', {

	// Selectors
	ignore: '[data-scroll-ignore]', // Selector for links to ignore (must be a valid CSS selector)
	header: null, // Selector for fixed headers (must be a valid CSS selector)
	topOnEmptyHash: true, // Scroll to the top of the page for links with href="#"

	// Speed & Duration
	speed: 500, // Integer. Amount of time in milliseconds it should take to scroll 1000px
	speedAsDuration: false, // If true, use speed as the total duration of the scroll animation
	durationMax: 500, // Integer. The maximum amount of time the scroll animation should take
	durationMin: null, // Integer. The minimum amount of time the scroll animation should take
	clip: true, // If true, adjust scroll distance to prevent abrupt stops near the bottom of the page
  offset: function () {
    if (window.innerWidth < 650) {
      console.log("less than 650");
    return 80

      }
      else if (window.innerWidth < 1000){
        console.log("less than 1000");
      return 100

      }
      else {
          console.log("more than 650");
        return 140
      }

  },

	// Easing
	easing: 'easeInOutCubic', // Easing pattern to use
	customEasing: function (time) {

		// Function. Custom easing pattern
		// If this is set to anything other than null, will override the easing option above

		// return <your formulate with time as a multiplier>

		// Example: easeInOut Quad
		return time < 0.5 ? 2 * time * time : -1 + (4 - 2 * time) * time;

	},

	// History
	updateURL: false, // Update the URL on scroll
	popstate: true, // Animate scrolling with the forward/backward browser buttons (requires updateURL to be true)

	// Custom Events
	emitEvents: true // Emit custom events

});




imagesLoaded( document.querySelector('.hero-image-container'), function( instance ) {
  console.log('all images are loaded');


setTimeout(function(){

  document.querySelector('.hero-image-container').classList.remove("loading-hero-image");

}, 500);



});



////SLIDER INIT

m_carousel();




////////


var hide_mobile_menu = function (event) {
  setTimeout(function(){
      document.getElementsByTagName("body")[0].classList.remove("mobile-menu-active");
   }, 500);


};
var hide_mobile_menu_fast = function (event) {

      document.getElementsByTagName("body")[0].classList.remove("mobile-menu-active");



};

document.addEventListener('scrollStart', hide_mobile_menu, false);
document.addEventListener('scrollStop', hide_mobile_menu_fast, false);
document.addEventListener('scrollCancel', hide_mobile_menu_fast, false);
