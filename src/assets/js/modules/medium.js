window.mediumInit = function (){

fetch('https://api.rss2json.com/v1/api.json?rss_url=https://medium.com/feed/maria-01')
   .then((res) => res.json())
   .then((data) => {
      // Filter for acctual posts. Comments don't have categories, therefore can filter for items with categories bigger than 0
      const res = data.items //This is an array with the content. No feed, no info about author etc..
      const posts = res.filter(item => item.categories.length > 0) // That's the main trick* !

      // Functions to create a short text out of whole blog's content
      function toText(node) {
         let tag = document.createElement('div')
         tag.innerHTML = node
         node = tag.innerText
         return node
      }
      function shortenText(text,startingPoint ,maxLength) {
         return text.length > maxLength?
         text.slice(startingPoint, maxLength):
         text
      }

      // Put things in right spots of markup
      let output = '';
      posts.slice(0, 8).forEach((item) => {
//         let date = item.pubDate;
//         var date_arr = item.pubDate.split(" ");
// var date_aar2 = date_arr[0].split("-");
// var monthIndex = date_aar2[1]
// var monthNames = ["January", "February", "March", "April", "May", "June",
//   "July", "August", "September", "October", "November", "December"
// ];
// var new_date = date_aar2[2] + "." + monthNames[monthIndex['month']] + "." + date_aar2[0];
// console.log(new_date);
// console.log(monthIndex);
let m_date = item.pubDate;

function format(input) {
    var dateFormat = { year: 'numeric', month: 'long', day: 'numeric' };
    return new Date(input.replace(/ /g,'T')).toLocaleDateString('en-US', dateFormat);
}
console.log(format(m_date));

         output += `

         <div class="cell small-6 medium-4 large-3 medium-fetch-cell">
            <a href="${item.link}" class="medium-embed-link" target="_blank">
            <div class="medium-embed-image">
               <img src="${item.thumbnail}" class=""></img>
               </div>
               <div class="m-content">
                      <span class="m-date">${format(m_date)}</span>
                     <h4 class="m-tittle">${shortenText(item.title)}</h4>


               </div>
            </a>
         </div>`

      })
      document.querySelector('.medium-fetch-grid').innerHTML = output
      document.querySelector('.medium-fetch-grid').classList.remove("medium-loading");
      // var loader = document.querySelector('.medium-loader');
      // loader.parentNode.removeChild(loader);

})

}
