

import Swiper from "swiper/dist/js/swiper.js";
  window.m_logo_carousel = function (){


// Params
let mainSliderSelector = '.logo-slider',
    // navSliderSelector = '.nav-slider',
    interleaveOffset = 0.5;

// Main Slider
let mainSliderOptions = {
      loop: false,
      speed:500,
      autoplay: false,
      slidesPerView: 'auto',
        spaceBetween: 6,

      // autoplay:{
      //   delay:3000
      // },

      grabCursor: true,
      watchSlidesProgress: true,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      on: {
        init: function(){
          this.autoplay.stop();
        },
        imagesReady: function(){
          this.el.classList.remove('loading');
          // this.autoplay.start();
        },

      }
    };

  let mainSlider = new Swiper(mainSliderSelector, mainSliderOptions);

    }
