
import Headroom from 'headroom.js';


export function headroom() {
//HEADROOM

// grab an element
const gheader = document.querySelector("header");
// construct an instance of Headroom, passing the element
window.gHeadRoom  = new Headroom(gheader,{
  "offset": 0,
  "tolerance": 0,
  onUnpin : function() {
const gheader = document.querySelector("header");
gheader.classList.remove("header-hide");

  },
  onNotTop : function() {

    document.body.classList.add("not--on-top")
  },

});
// initialise
gHeadRoom.init();
//HEADROOM ENDS
};
