<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>





<div id="directory-view-container" class="transition-fade">

	<?php if ( get_field( 'directory_tags' ) == 1 ) { ?>
 			 <div class="directory-labels">

 			<?php $tags = get_the_tags(); ?>

 	<?php if ($tags) { ?>
 			<button class="directory-label show-all" type="button" name="button" data-label="all">SHOW ALL</button>
 	<?php	foreach( $tags as $tag ) {  ?>
 			<button class="directory-label" type="button" name="button" data-label="'<?php echo $tag->name; ?>'"><?php echo $tag->name; ?></button>

 			<?php 	}
 						} ?>
 					</div>
 				<?php } ?>



 		<?php if ( have_rows( 'directory_items' ) ) : ?>
 			<div class="directory-items-wrapper grid-x small-up-2 medium-up-4 large-up-6">
 		<?php while ( have_rows( 'directory_items' ) ) : the_row(); ?>

 			<?php $tag_terms = get_sub_field( 'tag' ); ?>
 			<div class="cell directory-item active-d-item" data-label="<?php if ( $tag_terms ): ?><?php foreach ( $tag_terms as $tag_term ): ?>'<?php echo $tag_term->name; ?>',<?php endforeach; ?><?php endif; ?>">

 		<?php $link = get_sub_field( 'link' ); ?>
 		<?php if ( $link ) : ?>
 			<a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>">
 				<?php if ( get_sub_field( 'logo' ) ) { ?>
 					<div class="logo-image-wrapper">
 						<div class="logo-image">
 							<img class="" src="<?php the_sub_field( 'logo' ); ?>" />
 						</div>

 					</div>

 			<?php } ?>
 			<span><?php the_sub_field( 'name' ); ?></span></a>
 		<?php else : ?>


 					<div class="logo-image-wrapper">
 						<img class="logo-image" src="<?php the_sub_field( 'logo' ); ?>" />
 					</div>
 			<span><?php the_sub_field( 'name' ); ?></span>
 			<?php endif; ?>

 			</div>
 		<?php endwhile; ?>
 	<?php else : ?>
 		<?php // no rows found ?>

 	<?php endif; ?>

</div>


</div>


<?php get_footer();
