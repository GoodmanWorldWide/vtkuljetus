<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

    <!doctype html>
    <html lang="fi">

    <head>
        <meta charset="<?php bloginfo('charset'); ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="icon" type="image/png" href="kuvat/favicon-16x16.png" sizes="16x16">
<link rel="icon" type="image/png" href="kuvat/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="kuvat/favicon-96x96.png" sizes="96x96">

  <link href="https://fonts.googleapis.com/css?family=Muli:400,600,700,800&display=swap" rel="stylesheet">
        <?php wp_head(); ?>

        <script>
     (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
     (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
     m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
     })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

     ga('create', 'UA-106203420-1', 'auto');
     ga('send', 'pageview');

   </script>

    </head>

    <body <?php body_class(); ?>>

        <div id="vt-body">
          <div class="header-fake-bg">

          </div>
            <header id="desktop-header" class="main-header loading--fix" role="banner">
                <div class="top-nav grid-container">

                    <div class="grid-x header-grid">
                        <div class="cell small-4 large-2 m-logo-cell">
                          <a class="js-hover-disabler-link desktop-logo-link" data-scroll href="#koti">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 176.73 75.28"><title>vt</title><path d="M199.85,287.38a37.53,37.53,0,1,1,14.65-3A37.41,37.41,0,0,1,199.85,287.38Zm0-72.29a34.65,34.65,0,1,0,24.5,10.15A34.43,34.43,0,0,0,199.85,215.09Z" transform="translate(-162.21 -212.1)" style="fill:#ea420f"/><path d="M192.39,247L189,253.23,185.25,238a0.6,0.6,0,0,0-.58-0.45h-7.15a0.4,0.4,0,0,0-.39.49l6.48,26.11a0.5,0.5,0,0,0,.48.38h6.69a1.49,1.49,0,0,0,1.32-.79l9-16.77h-8.72Z" transform="translate(-162.21 -212.1)" style="fill:#ea420f"/><path d="M222.57,237.53H198.63a2,2,0,0,0-1.75,1l-3.65,6.81H206l-2.53,18.67a0.4,0.4,0,0,0,.39.45H211a0.5,0.5,0,0,0,.49-0.43L214,245.4h7.48A0.5,0.5,0,0,0,222,245l1-7A0.4,0.4,0,0,0,222.57,237.53Z" transform="translate(-162.21 -212.1)" style="fill:#ea420f"/><path d="M254,251.06l8.62-8.41a0.2,0.2,0,0,0-.14-0.34h-4.7a0.2,0.2,0,0,0-.14.06l-7.44,7.35,0.57-7.19a0.2,0.2,0,0,0-.2-0.22h-3.86a0.2,0.2,0,0,0-.2.19L245.18,260a0.2,0.2,0,0,0,.2.22h3.84a0.2,0.2,0,0,0,.2-0.18l0.6-7.57,6.8,7.69a0.2,0.2,0,0,0,.15.07h4.5a0.2,0.2,0,0,0,.15-0.33Z" transform="translate(-162.21 -212.1)" style="fill:#ea420f"/><path d="M274.58,247.35h-3.72a0.2,0.2,0,0,0-.2.18l-0.57,6.91a3.26,3.26,0,0,1-.72,2,2.12,2.12,0,0,1-1.68.7,1.71,1.71,0,0,1-1.31-.46,2.48,2.48,0,0,1-.43-1.9l0.59-7.18a0.2,0.2,0,0,0-.2-0.22H262.6a0.2,0.2,0,0,0-.2.19l-0.57,7.4c0,0.39,0,.68,0,0.85a4.83,4.83,0,0,0,1.12,3.43,4.29,4.29,0,0,0,3.26,1.19,5.65,5.65,0,0,0,2.35-.49,4.5,4.5,0,0,0,1.34-.92l-0.08,1a0.2,0.2,0,0,0,.2.22h3.62a0.2,0.2,0,0,0,.2-0.18l1-12.47A0.2,0.2,0,0,0,274.58,247.35Z" transform="translate(-162.21 -212.1)" style="fill:#ea420f"/><path d="M280.51,260L282,241.93a0.2,0.2,0,0,0-.2-0.22H278a0.2,0.2,0,0,0-.2.18L276.4,260a0.2,0.2,0,0,0,.2.22h3.71A0.2,0.2,0,0,0,280.51,260Z" transform="translate(-162.21 -212.1)" style="fill:#ea420f"/><path d="M288.06,247.35h-3.72a0.2,0.2,0,0,0-.2.18l-1,12.91a2,2,0,0,1-.58,1.4,1.87,1.87,0,0,1-1.28.43,4.84,4.84,0,0,1-1.13-.17,0.2,0.2,0,0,0-.25.18l-0.22,2.9a0.2,0.2,0,0,0,.12.2,5.89,5.89,0,0,0,2.17.33,5.07,5.07,0,0,0,3.73-1.36,5.87,5.87,0,0,0,1.56-3.79l1-13A0.2,0.2,0,0,0,288.06,247.35Z" transform="translate(-162.21 -212.1)" style="fill:#ea420f"/><path d="M288.63,241.49h-4a0.2,0.2,0,0,0-.2.19l-0.27,3.49a0.2,0.2,0,0,0,.2.22h4a0.2,0.2,0,0,0,.2-0.19l0.27-3.49A0.2,0.2,0,0,0,288.63,241.49Z" transform="translate(-162.21 -212.1)" style="fill:#ea420f"/><path d="M300.4,248.58a5.66,5.66,0,0,0-4.15-1.55,6.34,6.34,0,0,0-3.34.9,6.26,6.26,0,0,0-2.32,2.51,7.83,7.83,0,0,0-.83,3.65,6.6,6.6,0,0,0,.83,3.33,5.58,5.58,0,0,0,2.36,2.22,7.67,7.67,0,0,0,3.51.77,8.75,8.75,0,0,0,2.69-.42,6.26,6.26,0,0,0,2.16-1.16,0.2,0.2,0,0,0,.06-0.21l-0.79-2.51a0.2,0.2,0,0,0-.13-0.13,0.2,0.2,0,0,0-.18,0,5.91,5.91,0,0,1-3.57,1.22,2.9,2.9,0,0,1-2.2-.79,3.28,3.28,0,0,1-.76-2.16h8a0.2,0.2,0,0,0,.2-0.18,11,11,0,0,0,.07-1.25,5.8,5.8,0,0,0-1.56-4.29h0ZM293.94,252a2.34,2.34,0,0,1,2.45-2,2,2,0,0,1,1.53.57,2.15,2.15,0,0,1,.55,1.44h-4.54Z" transform="translate(-162.21 -212.1)" style="fill:#ea420f"/><path d="M312.22,247.35h-3l0.3-3.92a0.2,0.2,0,0,0-.26-0.21l-3.81,1.21a0.2,0.2,0,0,0-.14.18l-0.21,2.74H303a0.2,0.2,0,0,0-.2.18l-0.22,2.78a0.2,0.2,0,0,0,.2.22h2.14l-0.38,4.7a6.38,6.38,0,0,0,0,.66,4.12,4.12,0,0,0,4.56,4.53,6,6,0,0,0,2.24-.36,0.2,0.2,0,0,0,.12-0.17l0.25-2.88a0.2,0.2,0,0,0-.07-0.17,0.2,0.2,0,0,0-.18,0,2.4,2.4,0,0,1-2.41-.24,1.7,1.7,0,0,1-.36-1.37l0.38-4.66h3a0.2,0.2,0,0,0,.2-0.18l0.22-2.78A0.2,0.2,0,0,0,312.22,247.35Z" transform="translate(-162.21 -212.1)" style="fill:#ea420f"/><path d="M326,247.35h-3.72a0.2,0.2,0,0,0-.2.18l-0.57,6.91a3.26,3.26,0,0,1-.72,2,2.12,2.12,0,0,1-1.68.7,1.71,1.71,0,0,1-1.31-.46,2.48,2.48,0,0,1-.43-1.9l0.59-7.18a0.2,0.2,0,0,0-.2-0.22H314a0.2,0.2,0,0,0-.2.19l-0.57,7.4c0,0.39-.05.68-0.05,0.85a4.83,4.83,0,0,0,1.12,3.43,4.29,4.29,0,0,0,3.26,1.19,5.65,5.65,0,0,0,2.35-.49,4.51,4.51,0,0,0,1.34-.92l-0.08,1a0.2,0.2,0,0,0,.2.22H325a0.2,0.2,0,0,0,.2-0.18l1-12.47A0.2,0.2,0,0,0,326,247.35Z" transform="translate(-162.21 -212.1)" style="fill:#ea420f"/><path d="M332.24,250.38a3,3,0,0,1,1.43-.27,5.91,5.91,0,0,1,1.9.33,7,7,0,0,1,1.87,1,0.2,0.2,0,0,0,.3-0.08l1.18-2.49a0.2,0.2,0,0,0-.06-0.24,7.23,7.23,0,0,0-2.32-1.17,9.4,9.4,0,0,0-2.8-.43,7.58,7.58,0,0,0-2.92.53,4.67,4.67,0,0,0-2,1.49,3.66,3.66,0,0,0-.72,2.24,3,3,0,0,0,.68,2,4.27,4.27,0,0,0,1.56,1.15,17.53,17.53,0,0,0,2.24.75,12.39,12.39,0,0,1,1.85.68,0.71,0.71,0,0,1,.47.63,0.69,0.69,0,0,1-.39.62,2.88,2.88,0,0,1-1.42.27,7.49,7.49,0,0,1-2.3-.37,8.3,8.3,0,0,1-2.16-1.07,0.2,0.2,0,0,0-.3.08l-1.21,2.58a0.2,0.2,0,0,0,.08.26,11,11,0,0,0,5.81,1.56,6.89,6.89,0,0,0,4.07-1.08,3.56,3.56,0,0,0,1.55-3.05,3,3,0,0,0-.68-2,4.31,4.31,0,0,0-1.55-1.15,19.4,19.4,0,0,0-2.23-.77,9.94,9.94,0,0,1-1.83-.65,0.77,0.77,0,0,1-.51-0.68A0.7,0.7,0,0,1,332.24,250.38Z" transform="translate(-162.21 -212.1)" style="fill:#ea420f"/></svg>
                          </a>

                        </div>
                        <div class="cell auto nav-links n-l-r">
                            <div class="fake-ul">
                                <span class="fake-li"> <a data-scroll  href="#palvelut">Palvelut</a></span>
                                <span class="fake-li"> <a data-scroll href="#kalusto">Kalusto</a></span>
                                  <span class="fake-li"> <a data-scroll href="#meista">Tietoa meistä</a></span>
                                    <span class="fake-li"> <a data-scroll href="#yhteystiedot">Yhteystiedot</a></span>
                                        <span class="fake-li"> <a data-scroll href="#tarjous">Tarjouspyyntö</a></span>
                          </div>
                        </div>
                        <div class="cell shrink mobile-trigger-cell">
                            <button type="button" style="position:relative" name="mobile-menu" class="js-open-mobile-menu open-mobile-menu">
                                <svg id="open-menu" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 50 50" version="1.1" width="20px" height="20px"> <g> <path style=" " d="M 0 7.5 L 0 12.5 L 50 12.5 L 50 7.5 Z M 0 22.5 L 0 27.5 L 50 27.5 L 50 22.5 Z M 0 37.5 L 0 42.5 L 50 42.5 L 50 37.5 Z "></path> </g> </svg>
                                <svg id="close-menu" style="display: inline;" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 50 50" version="1.1" width="20px" height="20px"> <g> <path style=" " d="M 9.15625 6.3125 L 6.3125 9.15625 L 22.15625 25 L 6.21875 40.96875 L 9.03125 43.78125 L 25 27.84375 L 40.9375 43.78125 L 43.78125 40.9375 L 27.84375 25 L 43.6875 9.15625 L 40.84375 6.3125 L 25 22.15625 Z "></path> </g> </svg>
                            </button>
                        </div>


                    </div>
                </div>



            </header>


            <div id="mobile-menu-id" class="mobile-menu">
                <div class="mobile-menu-inner">


                    <div class="flex-container flex-dir-column mm-flex-wrapper">

                        <div class="mobile-nav-links">
                            <div class="mobile-nav-links-inner">
                              <div class="fake-link-container">
                                <span class="fake-li"> <a data-scroll  href="#palvelut">Palvelut</a></span>
                                <span class="fake-li"> <a data-scroll href="#kalusto">Kalusto</a></span>
                                  <span class="fake-li"> <a data-scroll href="#meista">Tietoa meistä</a></span>
                                    <span class="fake-li"> <a data-scroll href="#yhteystiedot">Yhteystiedot</a></span>
                                        <span class="fake-li"> <a data-scroll href="#tarjous">Tarjouspyyntö</a></span>
                            </div>

                    </div>
                </div>

            </div>
            </div>
              </div>
