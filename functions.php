<?php
/**
 * Author: Ole Fredrik Lie
 * URL: http://olefredrik.com
 *
 * FoundationPress functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

/** Various clean up functions */
require_once( 'library/cleanup.php' );

/** Required for Foundation to work properly */
require_once( 'library/foundation.php' );

/** Format comments */
require_once( 'library/class-foundationpress-comments.php' );

/** Register all navigation menus */
require_once( 'library/navigation.php' );

/** Add menu walkers for top-bar and off-canvas */
require_once( 'library/class-foundationpress-top-bar-walker.php' );
require_once( 'library/class-foundationpress-mobile-walker.php' );

/** Create widget areas in sidebar and footer */
require_once( 'library/widget-areas.php' );

/** Return entry meta information for posts */
require_once( 'library/entry-meta.php' );

/** Enqueue scripts */
require_once( 'library/enqueue-scripts.php' );

/** Add theme support */
require_once( 'library/theme-support.php' );

/** Add Nav Options to Customer */
require_once( 'library/custom-nav.php' );

/** Change WP's sticky post class */
require_once( 'library/sticky-posts.php' );

/** Configure responsive image sizes */
require_once( 'library/responsive-images.php' );

/** Gutenberg editor support */
require_once( 'library/gutenberg.php' );

/** If your site requires protocol relative url's for theme assets, uncomment the line below */
// require_once( 'library/class-foundationpress-protocol-relative-theme-assets.php' );


//ACF OPTIONS
// if( function_exists('acf_add_options_page') ) {
//
// 	acf_add_options_page('Navigation/Footer');
//
//
// }


function add_linebreak_shortcode() {
return '<br />';
}
add_shortcode('br', 'add_linebreak_shortcode' );


/**
 *  Remove the h1 tag from the WordPress editor.
 *
 *  @param   array  $settings  The array of editor settings
 *  @return  array             The modified edit settings
 */

function my_format_TinyMCE( $in ) {
        $in['block_formats'] = "Paragraph=p; Heading=h3;Preformatted=pre";
    return $in;
}
add_filter( 'tiny_mce_before_init', 'my_format_TinyMCE' );


// add_image_size( 'm-split-small', 600, 600, false );
// add_image_size( 'm-split', 1100, 1100, false );
//
//
// add_image_size( 'm-full-medium', 1200, 675, true );
// add_image_size( 'm-full', 2048, 1152, true );
//
//
// add_image_size( 'm-team', 400, 600, true );









add_image_size( 'frost', 50, 50 );
add_image_size( 'carousel-image', 1400, 2000 );

// Disbale src-set

function disable_wp_responsive_images() {
	return 1;
}
add_filter('max_srcset_image_width', 'disable_wp_responsive_images');





add_filter( 'image_size_names_choose', 'add_image_insert_override' );







//// OPTIONS PAGE

// 
// function register_acf_options_pages() {
//
//     // Check function exists.
//     if( !function_exists('acf_add_options_page') )
//         return;
//
//     // register options page.
//     $option_page = acf_add_options_page(array(
//         'page_title'    => __('FOOTER INFO'),
//         'menu_title'    => __('FOOTER INFO'),
//         'menu_slug'     => 'footer-info',
//         'capability'    => 'edit_posts',
//         'redirect'      => false
//     ));
// }

// Hook into acf initialization.
add_action('acf/init', 'register_acf_options_pages');





add_filter( 'big_image_size_threshold', '__return_false' );
