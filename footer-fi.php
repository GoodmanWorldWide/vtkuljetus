<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */
?>

<footer class="footer-alava">
	<div class="footer-texture flow-item"  data-rellax-speed="7" data-rellax-percentage="0.1">
			<img src="<?php echo get_template_directory_uri(); ?>/dist/assets/images/texture.svg" alt="">
	</div>
<div class="grid-container flex-container footer-content">
<div class="footer-a-logo-cell">
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64.26 72.38"><title>Untitled-2</title><path d="M250.13,213.47A32.16,32.16,0,0,0,218,245.6v8.12a32.13,32.13,0,1,0,64.26,0V245.6A32.16,32.16,0,0,0,250.13,213.47Zm30.06,40.25a30.06,30.06,0,1,1-60.12,0V245.6a30.06,30.06,0,0,1,60.12,0v8.12Z" transform="translate(-218 -213.47)" style="fill:#f3bbbb"/><path d="M264.61,261l-11.9-29h-9v1a3,3,0,0,1,2.57,2.1l0.7,1.63-8.46,20.45c-1.72,4.15-3.55,6.31-5.54,6.52v1H245.2v-1c-3.92-.28-4.94-1.59-4.94-3.93a5.82,5.82,0,0,1,.47-2.19l1.87-4.64,5.84,0.85c3.13,0.46,6.23,1.42,7.75,5.32l0.49,1.18a4.72,4.72,0,0,1,.37,1.59,1.69,1.69,0,0,1-.51,1.28,2.24,2.24,0,0,1-1.49.54v1h12.36v-1a2.34,2.34,0,0,1-1.63-.79A6,6,0,0,1,264.61,261Zm-15.89-9.11-5.38-.78,4.74-11.75,5.81,14.16A14.36,14.36,0,0,0,248.72,251.87Z" transform="translate(-218 -213.47)" style="fill:#f3bbbb"/></svg>
</div>

<div class="footer-info-cell">
<p><?php the_field( 'footer_info_1_fi', 'option' ); ?></p>
</div>
<div class="footer-info-cell">
<p><?php the_field( 'footer_info_2_fi', 'option' ); ?></p>
</div>
<div class="footer-info-cell">
<p><?php the_field( 'footer_info_3_fi', 'option' ); ?></p>
</div>
<div class="footer-social-link flex-container align-right">

	<div class="">
<p><?php the_field( 'footer_info_4_fi', 'option' ); ?></p>
	</div>
</div>
</div>
</footer>

<div class="brand-page-overlay">
	<div class="brand-wrapper">


<div class="pjax-container">

</div>

	</div>
</div>

<a href="<?php echo esc_url(home_url('/')); ?>" class="brand-link back-to-home-link"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.45 19.45"><title>Untitled-2</title><line x1="1.41" y1="18.03" x2="18.03" y2="1.41" style="fill:none;stroke:#1a1a1a;stroke-miterlimit:10;stroke-width:4px"/><line x1="18.03" y1="18.03" x2="1.41" y2="1.41" style="fill:none;stroke:#1a1a1a;stroke-miterlimit:10;stroke-width:4px"/></svg></a>

</div>


		<script type="text/javascript">
		var supportsES6 = function() {
					try {
					new Function("(a = 0) => a");
					return true;
					}
					catch (err) {
					return false;
					}
					}();


					if (supportsES6) {

					}
					else {


					}
		</script>
<?php wp_footer(); ?>



</body>
</html>
