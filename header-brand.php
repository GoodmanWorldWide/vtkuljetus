<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

    <!doctype html>
    <html lang="en">

    <head>
        <meta charset="<?php bloginfo('charset'); ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />



        <style>
          @font-face {font-family: 'Editor-Extrabold';src: url('<?php echo get_template_directory_uri(); ?>/fonts/editor/webfonts/3A432E_0_0.eot');src: url('<?php echo get_template_directory_uri(); ?>/fonts/editor/webfonts/3A432E_0_0.eot?#iefix') format('embedded-opentype'),url('<?php echo get_template_directory_uri(); ?>/fonts/editor/webfonts/3A432E_0_0.woff2') format('woff2'),url('<?php echo get_template_directory_uri(); ?>/fonts/editor/webfonts/3A432E_0_0.woff') format('woff'),url('<?php echo get_template_directory_uri(); ?>/fonts/editor/webfonts/3A432E_0_0.ttf') format('truetype');}
        @font-face {font-family: 'MessinaSansMono_Web_SemiBold';src: url('<?php echo get_template_directory_uri(); ?>/fonts/MessinaSansMono_Web_SemiBold/MessinaSansMonoWeb-SemiBold.eot');src: url('<?php echo get_template_directory_uri(); ?>/fonts/MessinaSansMono_Web_SemiBold/MessinaSansMonoWeb-SemiBold.eot?#iefix') format('embedded-opentype'),url('<?php echo get_template_directory_uri(); ?>/fonts/MessinaSansMono_Web_SemiBold/MessinaSansMonoWeb-SemiBold.woff2') format('woff2'),url('<?php echo get_template_directory_uri(); ?>/fonts/MessinaSansMono_Web_SemiBold/MessinaSansMonoWeb-SemiBold.woff') format('woff'),url('<?php echo get_template_directory_uri(); ?>/fonts/MessinaSansMono_Web_SemiBold/MessinaSansMonoWeb-SemiBold.ttf') format('truetype');}

                @font-face {font-family: 'MessinaSans_Web_Regular';src: url('<?php echo get_template_directory_uri(); ?>/fonts/MessinaSans_Web_Regular/MessinaSansWeb-Regular.eot');src: url('<?php echo get_template_directory_uri(); ?>/fonts/MessinaSans_Web_Regular/MessinaSansWeb-Regular.eot?#iefix') format('embedded-opentype'),url('<?php echo get_template_directory_uri(); ?>/fonts/MessinaSans_Web_Regular/MessinaSansWeb-Regular.woff2') format('woff2'),url('<?php echo get_template_directory_uri(); ?>/fonts/MessinaSans_Web_Regular/MessinaSansWeb-Regular.woff') format('woff'),url('<?php echo get_template_directory_uri(); ?>/fonts/MessinaSans_Web_Regular/MessinaSansWeb-Regular.ttf') format('truetype');}

                @font-face {font-family: 'MessinaSans_Bold';src: url('<?php echo get_template_directory_uri(); ?>/fonts/messina/messina.eot');src: url('<?php echo get_template_directory_uri(); ?>/fonts/messina/messina.eot?#iefix') format('embedded-opentype'),url('<?php echo get_template_directory_uri(); ?>/fonts/messina/messina.woff2') format('woff2'),url('<?php echo get_template_directory_uri(); ?>/fonts/messina/messina.woff') format('woff'),url('<?php echo get_template_directory_uri(); ?>/fonts/messina/messina.ttf') format('truetype');}

        </style>



        <?php wp_head(); ?>

    </head>

    <body <?php body_class(); ?>>

        <div id="alava-body">
