<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

    <!doctype html>
    <html lang="fi">

    <head>
        <meta charset="<?php bloginfo('charset'); ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />



    <?php get_template_part( 'template-parts/content', 'fonts' ); ?>



        <?php wp_head(); ?>

    </head>

    <body <?php body_class("loading-alava-page"); ?>>

        <div id="alava-body">
            <div class="header-fake-bg on-top">

            </div>
            <header id="desktop-header" class="main-header" role="banner">
                <div class="top-nav grid-container">

                    <div class="grid-x header-grid">
                        <div class="cell small-4 large-2 m-logo-cell">
                          <a class="js-hover-disabler-link desktop-logo-link" href="<?php echo esc_url(home_url('/')); ?>">
                              <svg class="anim a-1"xmlns="http://www.w3.org/2000/svg" viewBox="0 0 284 59.37"><title>logo</title><path d="M57.57,52.66L35.89,0H19.56V1.78Q22.62,2,24.24,5.6l1.28,3L10.1,45.73C7,53.28,3.63,57.19,0,57.58v1.78H22.19V57.58c-7.14-.51-9-2.9-9-7.14a10.56,10.56,0,0,1,.85-4L17.46,38l10.65,1.55c5.7,0.83,11.35,2.58,14.12,9.68l0.88,2.15a8.56,8.56,0,0,1,.68,2.88,3.08,3.08,0,0,1-.94,2.33,4.09,4.09,0,0,1-2.72,1v1.78H62.67V57.58a4.27,4.27,0,0,1-3-1.44A11,11,0,0,1,57.57,52.66Zm-29-16.56-9.8-1.42,8.64-21.36L38,39a26.22,26.22,0,0,0-9.39-2.9h0Z" style="fill:#1a1a1a"/><path d="M175.27,52.66L153.58,0H137.25V1.78q3.06,0.17,4.68,3.82l1.28,3L127.79,45.73c-3.13,7.55-6.47,11.46-10.1,11.86v1.78h22.19V57.58c-7.14-.51-9-2.9-9-7.14a10.56,10.56,0,0,1,.85-4L135.15,38l10.65,1.55c5.7,0.83,11.35,2.58,14.12,9.68l0.88,2.15a8.56,8.56,0,0,1,.68,2.88,3.08,3.08,0,0,1-.94,2.33,4.09,4.09,0,0,1-2.72,1v1.78h22.53V57.58a4.27,4.27,0,0,1-3-1.44A11,11,0,0,1,175.27,52.66Zm-29-16.56-9.8-1.42,8.64-21.36L155.73,39a26.22,26.22,0,0,0-9.42-2.9h0Z" style="fill:#1a1a1a"/><path d="M281,56.14a11,11,0,0,1-2.13-3.48L257.21,0H240.89V1.78q3.06,0.17,4.68,3.82l1.28,3L231.42,45.73c-3.13,7.55-6.47,11.46-10.1,11.86v1.78h22.19V57.58c-7.14-.51-9-2.9-9-7.14a10.56,10.56,0,0,1,.85-4L238.79,38l10.65,1.55c5.7,0.83,11.36,2.58,14.12,9.68l0.88,2.15a8.56,8.56,0,0,1,.68,2.88,3.08,3.08,0,0,1-.94,2.33,4.09,4.09,0,0,1-2.72,1v1.78H284V57.58A4.27,4.27,0,0,1,281,56.14Zm-31.08-20-9.8-1.42,8.64-21.36L259.36,39a26.23,26.23,0,0,0-9.42-2.9Z" style="fill:#1a1a1a"/><path d="M115.91,38.25a43,43,0,0,1-12.46,13.87q-7.36,5.13-14.75,5.13H88.53a2.16,2.16,0,0,1-1.66-.59A2.26,2.26,0,0,1,86.31,55V4.66a2.53,2.53,0,0,1,.85-2.16,4.64,4.64,0,0,1,2.55-.72V0H69.82V1.78a4.64,4.64,0,0,1,2.55.72,2.53,2.53,0,0,1,.85,2.16v50a2.53,2.53,0,0,1-.85,2.16,4.63,4.63,0,0,1-2.55.72v1.78h39L118,38.25h-2.09Z" style="fill:#1a1a1a"/><path d="M231.5,1.78V0H209.39V1.78c3.51,0.23,5.33.93,7.11,2.12s2.2,2.83,2.2,4.92a11.06,11.06,0,0,1-.85,4l-12.23,30L192.21,7.89a7.38,7.38,0,0,1-.68-2.8,3,3,0,0,1,1-2.33,4.41,4.41,0,0,1,2.85-1V0H172.82V1.78q3.23,0.25,5,4.92L199,59.36h3.91l18.54-45.71Q226.14,2.29,231.5,1.78Z" style="fill:#1a1a1a"/></svg>
                          </a>

                        </div>
                        <div class="cell auto nav-links n-l-r">
                            <div class="fake-ul">
                                <span class="fake-li"> <a data-scroll href="#esittely">Esittely</a></span>
                                <span class="fake-li"> <a data-scroll href="#valikoima">Valikoima</a></span>
                                  <span class="fake-li"> <a data-scroll href="#sijainti">Sijainti</a></span>
                                    <span class="fake-li"> <a data-scroll href="#aukiolo">Aukioloajat</a></span>
                                        <span class="fake-li"> <a data-scroll href="#yhteystiedot">Yhteystiedot</a></span>
                          </div>
                        </div>
                        <div class="cell shrink mobile-trigger-cell">
                            <button type="button" name="mobile-menu" class="js-open-mobile-menu open-mobile-menu">
                                <span class="mm-icon-container">
      <span class="mm-icon-inner">

        </span>
                                </span>
                            </button>
                        </div>
                        <div class="cell small-4 large-2 language-switcher flex-container align-middle align-right">
                          <span class="fake-li active-lang"> <a href="<?php echo esc_url(home_url('/')); ?>">FI</a></span>
                          <span class="lang-separator"></span>
                          <span class="fake-li"> <a href="<?php echo esc_url(home_url('/')); ?>en/">EN</a></span>
                        </div>

                    </div>
                </div>



            </header>


            <div id="mobile-menu-id" class="mobile-menu">
                <div class="mobile-menu-inner">


                    <div class="flex-container flex-dir-column mm-flex-wrapper">

                        <div class="mobile-nav-links">
                            <div class="mobile-nav-links-inner">
                              <div class="fake-link-container">
                                <span class="fake-li"> <a data-scroll href="#esittely">Esittely</a></span>
                                <span class="fake-li"> <a data-scroll href="#valikoima">Valikoima</a></span>
                                  <span class="fake-li"> <a data-scroll href="#sijainti">Sijainti</a></span>
                                    <span class="fake-li"> <a data-scroll href="#aukiolo">Aukioloajat</a></span>
                                        <span class="fake-li"> <a data-scroll href="#yhteystiedot">Yhteystiedot</a></span>
                            </div>

                    </div>
                </div>

            </div>
            </div>
              </div>
