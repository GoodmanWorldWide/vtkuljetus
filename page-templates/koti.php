<?php
/*
Template Name: Kotisivu
*/
get_header(); ?>

<div id="koti" class="main-app">

<section class="section--hero grid-container">
	<div class="hero-info-container">
			<h1><?php the_field( 'alkuteksti' ); ?></h1>
			<a data-scroll href="#tarjous" class="hero-contact-hash">
				Pyydä tarjous!
			</a>
	</div>
	<div class="hero-image-container loading-hero-image">
		<?php $alkukuva = get_field( 'alkukuva' ); ?>
		<div class="frost-loader">
	<img src="<?php echo $alkukuva['sizes']['frost']; ?>" alt="<?php echo $alkukuva['alt']; ?>" />
		</div>

		<div class="hero-image-container--inner">
	<img src="<?php echo $alkukuva['url']; ?>" alt="<?php echo $alkukuva['alt']; ?>" />
		</div>


	</div>


</section>

<section class="section--services grid-container">
<div id="palvelut" class="section-header-wrapper">
<h2 class="section-header">Palvelut</h2>
</div>

<div class="grid-x services-grid content-spacer-top--big">
	<?php if ( have_rows( 'palvelu' ) ) : ?>
	<?php while ( have_rows( 'palvelu' ) ) : the_row(); ?>
	<div class="cell medium-6 service-cell">
	<span><?php the_sub_field( 'icon_svg' ); ?></span>
	<h3><?php the_sub_field( 'palvelun_nimi' ); ?></h3>
	<p><?php the_sub_field( 'palvelun_seloste' ); ?></p>
	</div>
	<?php endwhile; ?>
<?php endif; ?>


</div>
</section>
<section class="section--gear grid-container">
<div id="kalusto" class="section-header-wrapper">
<h2 class="section-header">Kalusto</h2>
</div>

<div class="grid-x  content-spacer-top--small">
<div class="cell gear-slider-cell">
	<div class="swiper-ratio-box">
	<div class="swiper-container b-swiper-container main-slider loading">
		<div class="swiper-wrapper">
			<?php if ( have_rows( 'kuvat' ) ) : ?>
				<?php while ( have_rows( 'kuvat' ) ) : the_row(); ?>
					<?php $kuva = get_sub_field( 'kuva' ); ?>
					<?php if ( $kuva ) { ?>
						<div class="swiper-slide b-swiper-slide">
			 			 <figure class="slide-bgimg" style="background-image:url(<?php echo $kuva['sizes']['carousel-image']; ?>)">
			 				 <img class="entity-img" src="<?php echo $kuva['sizes']['carousel-image']; ?>" alt="VT-KULJETUS">
			 			 </figure>

			 		 </div>

					<?php } ?>
				<?php endwhile; ?>
			<?php else : ?>
				<?php // no rows found ?>
			<?php endif; ?>
	 </div>

	 <div class="swiper-button-prev swiper-button-white"></div>
	<div class="swiper-button-next swiper-button-white"></div>
		</div>
			</div>
				<div class="swiper-pagination"></div>
</div>
<div class="cell gear-info-cell">
<p><?php the_field( 'kalusto_teksti' ); ?></p>
</div>

</div>
</section>
<section class="section--about grid-container">
<div id="meista" class="section-header-wrapper">
<h2 class="section-header">Tietoa meistä</h2>
</div>

<div class="grid-x content-spacer-top--small">
	<div class="cell about-info-cell">
	<p><?php the_field( 'teksti_tietoa' ); ?></p>
	</div>
<div class="cell about-image-cell">
	<?php $kuva_tietoa = get_field( 'kuva_tietoa' ); ?>
<?php if ( $kuva_tietoa ) { ?>
	<img src="<?php echo $kuva_tietoa['sizes']['carousel-image']; ?>" alt="<?php echo $kuva_tietoa['alt']; ?>" />
<?php } ?>

</div>


</div>
</section>
<section class="section--contact grid-container">
<div class="grid-x content-spacer-top--small">
	<div class="cell large-6 contact-cell">
		<div id="yhteystiedot" class="section-header-wrapper">
		<h2 class="section-header">Yhteystiedot</h2>
		</div>
		<div class="contact-cell-inner content-spacer-top--small contact-wysiwyg-block">
<?php the_field( 'yhteystiedot_sisalto' ); ?>
	</div>
	</div>
	<div class="cell large-6 contact-cell">
		<div id="tarjous" class="section-header-wrapper">
		<h2 class="section-header">Tarjouspyyntö</h2>
		</div>
		<div class="contact-cell-inner content-spacer-top--small">
<?php the_field( 'tarjouspyynto' ); ?>
	</div>


		</div>



</div>
</section>




</div>

<?php get_footer();
