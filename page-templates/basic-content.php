<?php
/*
Template Name: Basic Content
*/
get_header(); ?>

<div class="main-app">
<?php get_template_part( 'template-parts/page', 'hero' ); ?>
<?php get_template_part( 'template-parts/page', 'content' ); ?>
</div>

<?php get_footer();
