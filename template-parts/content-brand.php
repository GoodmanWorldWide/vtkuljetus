<?php
/**
 * The default template for displaying page content
 *
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<div class="brand-wrapper">
<div class="pjax-container">
<h3><?php the_title( '' ); ?></h3>
<?php if ( have_rows( 'link_to_brands_home_page' ) ) : ?>
	<?php while ( have_rows( 'link_to_brands_home_page' ) ) : the_row(); ?>
		<a href="<?php the_sub_field( 'actual_url' ); ?>" target="_blank"><?php the_sub_field( 'link_text' ); ?></a>
	<?php endwhile; ?>
<?php endif; ?>
<div class="brand-content loading-article-body">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
 <?php the_content(); ?>
<?php endwhile; ?>
<?php endif; ?>
