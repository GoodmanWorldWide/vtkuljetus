<?php
/**
 * PAGE team section
 *
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<div class="section-break">
	<div class="section-break-inner">

	</div>
</div>
<section class="page-team-section">
		<div class="grid-container">

<h2 class="section-h2">WE ARE THE HOSPITAL CARETAKERS</h2>

			<div class="grid-x t-member-grid align-center small-up-2 medium-up-4 large-up-5 xlarge-up-6">
				<?php if ( have_rows( 'team_member' ) ) : ?>
	<?php while ( have_rows( 'team_member' ) ) : the_row(); ?>
		<div class="cell t-member">
		<?php $image = get_sub_field( 'image' ); ?>
		<?php if ( $image ) { ?>
			<img src="<?php echo $image['sizes']['m-team']; ?>" alt="<?php the_title(); ?>" style="width:auto;">

		<?php } ?>
		<p class="t-member-name"><?php the_sub_field( 'name_' ); ?></p>
		<p class="t-member-title"><?php the_sub_field( 'title' ); ?></p>
		<p class="t-member-email"><?php the_sub_field( 'email' ); ?></p>
		<?php if ( have_rows( 'social_links' ) ) : ?>
			<div class="t-member-social">
			<?php while ( have_rows( 'social_links' ) ) : the_row(); ?>
				<?php $twitter = get_sub_field( 'twitter' ); ?>
				<?php if ( $twitter ) { ?>
					<a href="<?php echo $twitter['url']; ?>" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 33.5 27.22"><title>twitter</title><path d="M1544,1763.54c12.64,0,19.55-10.47,19.55-19.55,0-.3,0-0.59,0-0.89a14,14,0,0,0,3.43-3.56,13.72,13.72,0,0,1-3.95,1.08,6.9,6.9,0,0,0,3-3.8,13.77,13.77,0,0,1-4.36,1.67,6.88,6.88,0,0,0-11.71,6.27,19.51,19.51,0,0,1-14.16-7.18,6.88,6.88,0,0,0,2.13,9.17,6.82,6.82,0,0,1-3.12-.86V1746a6.87,6.87,0,0,0,5.51,6.74,6.86,6.86,0,0,1-3.1.12,6.88,6.88,0,0,0,6.42,4.77,13.79,13.79,0,0,1-8.53,2.95,14,14,0,0,1-1.64-.1,19.46,19.46,0,0,0,10.53,3.08" transform="translate(-1533.5 -1736.32)" style="fill:black"/></svg></a>
				<?php } ?>
				<?php $linkedin = get_sub_field( 'linkedin' ); ?>
				<?php if ( $linkedin ) { ?>
					<a href="<?php echo $linkedin['url']; ?>" target="<?php echo $linkedin['target']; ?>"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M19 0h-14c-2.761 0-5 2.239-5 5v14c0 2.761 2.239 5 5 5h14c2.762 0 5-2.239 5-5v-14c0-2.761-2.238-5-5-5zm-11 19h-3v-11h3v11zm-1.5-12.268c-.966 0-1.75-.79-1.75-1.764s.784-1.764 1.75-1.764 1.75.79 1.75 1.764-.783 1.764-1.75 1.764zm13.5 12.268h-3v-5.604c0-3.368-4-3.113-4 0v5.604h-3v-11h3v1.765c1.396-2.586 7-2.777 7 2.476v6.759z"/></svg></a>
				<?php } ?>
			<?php endwhile; ?>
				</div>
		<?php endif; ?>
		</div>
	<?php endwhile; ?>
<?php else : ?>
	<?php // no rows found ?>
<?php endif; ?>


</div>

		</div>
		</section>
