<?php
/**
 * The default template for displaying page content
 *
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<style>
	@font-face {font-family: 'editor-extrabold';src: url('<?php echo esc_url(home_url('/')); ?>fonts/editor/webfonts/3a432e_0_0.eot');src: url('<?php echo esc_url(home_url('/')); ?>fonts/editor/webfonts/3a432e_0_0.eot?#iefix') format('embedded-opentype'),url('<?php echo esc_url(home_url('/')); ?>fonts/editor/webfonts/3a432e_0_0.woff2') format('woff2'),url('<?php echo esc_url(home_url('/')); ?>fonts/editor/webfonts/3a432e_0_0.woff') format('woff'),url('<?php echo esc_url(home_url('/')); ?>fonts/editor/webfonts/3a432e_0_0.ttf') format('truetype');}
@font-face {font-family: 'messinasansmono_web_semibold';src: url('<?php echo esc_url(home_url('/')); ?>fonts/messinasansmono_web_semibold/messinasansmonoweb-semibold.eot');src: url('<?php echo esc_url(home_url('/')); ?>fonts/messinasansmono_web_semibold/messinasansmonoweb-semibold.eot?#iefix') format('embedded-opentype'),url('<?php echo esc_url(home_url('/')); ?>messinasansmonowebsemibold.woff2') format('woff2'),url('<?php echo esc_url(home_url('/')); ?>fonts/messinasansmono_web_semibold/messinasansmonoweb-semibold.woff') format('woff'),url('<?php echo esc_url(home_url('/')); ?>fonts/messinasansmono_web_semibold/messinasansmonoweb-semibold.ttf') format('truetype');}

				@font-face {font-family: 'messinasans_web_regular';src: url('<?php echo esc_url(home_url('/')); ?>fonts/messinasans_web_regular/messinasansweb-regular.eot');src: url('<?php echo esc_url(home_url('/')); ?>fonts/messinasans_web_regular/messinasansweb-regular.eot?#iefix') format('embedded-opentype'),url('<?php echo esc_url(home_url('/')); ?>messinasansweb-regular.woff2') format('woff2'),url('<?php echo esc_url(home_url('/')); ?>fonts/messinasans_web_regular/messinasansweb-regular.woff') format('woff'),url('<?php echo esc_url(home_url('/')); ?>fonts/messinasans_web_regular/messinasansweb-regular.ttf') format('truetype');}

				@font-face {font-family: 'messinasans_bold';src: url('<?php echo esc_url(home_url('/')); ?>1.eot');src: url('<?php echo esc_url(home_url('/')); ?>1.eot?#iefix') format('embedded-opentype'),url('<?php echo esc_url(home_url('/')); ?>1.woff2') format('woff2'),url('/fonts/messina/messina.woff') format('woff'),url('/fonts/messina/messina.ttf') format('truetype');}

</style>
