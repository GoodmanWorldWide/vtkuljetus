<?php
/*
Template Name: Brand
*/
get_header("brand"); ?>
<?php get_template_part( 'template-parts/content', 'brand' ); ?>
<a href="<?php echo esc_url(home_url('/')); ?>" class="brand-link close-brand-view">Sulje esittely</a>
</div>

</div>
</div>
<?php get_footer("brand");
