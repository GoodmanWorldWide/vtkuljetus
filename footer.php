<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */
?>

<div class="vt-footer">
<div class="grid-container">
<div class="grid-x">
<div class="cell footer-icon-cell">
  <div class="footer-svg-box">
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 75.28 75.28"><title>vt</title><path d="M199.85,287.38a37.53,37.53,0,1,1,14.65-3A37.41,37.41,0,0,1,199.85,287.38Zm0-72.29a34.65,34.65,0,1,0,24.5,10.15A34.43,34.43,0,0,0,199.85,215.09Z" transform="translate(-162.21 -212.1)" style="fill:white"/><path d="M192.39,247L189,253.23,185.25,238a0.6,0.6,0,0,0-.58-0.45h-7.15a0.4,0.4,0,0,0-.39.49l6.48,26.11a0.5,0.5,0,0,0,.48.38h6.69a1.49,1.49,0,0,0,1.32-.79l9-16.77h-8.72Z" transform="translate(-162.21 -212.1)" style="fill:white"/><path d="M222.57,237.53H198.63a2,2,0,0,0-1.75,1l-3.65,6.81H206l-2.53,18.67a0.4,0.4,0,0,0,.39.45H211a0.5,0.5,0,0,0,.49-0.43L214,245.4h7.48A0.5,0.5,0,0,0,222,245l1-7A0.4,0.4,0,0,0,222.57,237.53Z" transform="translate(-162.21 -212.1)" style="fill:white"/></svg>
  </div>
  <div class="footer-name-box">
    <span style="color:white">VT-Kuljetus & Monipalvelu Oy</span>
  </div>

</div>
<div class="cell footer-some-cell">
<span>Seuraa meitä:</span><span class="fb-icon"> <a href="https://www.facebook.com/vtkuljetusjamonipalvelu/" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28.11 28.11"><title>fb-icon</title><path d="M263.45,235.21h-25a1.55,1.55,0,0,0-1.55,1.55v25a1.55,1.55,0,0,0,1.55,1.55h13.46V252.43h-3.66v-4.24h3.66v-3.13c0-3.63,2.22-5.61,5.46-5.61a30.24,30.24,0,0,1,3.27.17v3.79h-2.25c-1.76,0-2.1.84-2.1,2.07v2.71h4.2l-0.55,4.24h-3.65v10.88h7.16a1.55,1.55,0,0,0,1.55-1.55v-25a1.55,1.55,0,0,0-1.55-1.55" transform="translate(-236.89 -235.21)" style="fill:#fff"/></svg></a></span>
</div>
</div>
</div>
</div>

</div>


		<script type="text/javascript">
		var supportsES6 = function() {
					try {
					new Function("(a = 0) => a");
					return true;
					}
					catch (err) {
					return false;
					}
					}();


					if (supportsES6) {

					}
					else {


					}
		</script>
<?php wp_footer(); ?>



</body>
</html>
